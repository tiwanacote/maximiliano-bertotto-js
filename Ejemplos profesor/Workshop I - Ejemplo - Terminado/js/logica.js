const cargoZonaDeResidencia = ()=> {
      let optionZV
         for (let zonaV of zonaVivienda) {
            optionZV += `<option value="${zonaV.factor}">${zonaV.area}</option>`
         }
         return optionZV
}

const cargoTiposDeVivienda = ()=> {
      let optTV
         for (let tipoV of tipoVivienda) {
            optTV += `<option value="${tipoV.factor}">${tipoV.tipo}</option>`
         }
         return optTV
}

const faltanCargarDatos = ()=> {
      return (isNaN(metros2.value) || selectTipoVivienda.value.trim() == "" || selectZonaVivienda.value.trim() == "")
}

const cotizarSeguroVivienda = ()=> {
            if (faltanCargarDatos()) {
                alert("Complete toda la información solicitada para poder cotizar.")
                return
            } else {
                mts = parseInt(metros2.value)
                zonaViv = parseFloat(selectZonaVivienda.value)
                tipoViv = parseFloat(selectTipoVivienda.value)
                valorDeLaCuota = csh.valorDePoliza(mts, zonaViv, tipoViv)
                valorCuota.innerText = `$ ${valorDeLaCuota}`
            }
}