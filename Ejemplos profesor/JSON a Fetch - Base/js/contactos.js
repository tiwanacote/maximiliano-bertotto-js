//CARGAR LA TABLA CONTACTOS CON LOS DATOS DEL ARRAY JSON
const cargoGrillaContactos = (arr)=> {
      let detalleTabla = ""
      if (arr.length > 0) {
         arr.forEach(contacto => {
             detalleTabla += `<tr>
                                 <td>${contacto.id}</td>
                                 <td>${contacto.nombre}</td>
                                 <td>${contacto.telefono}</td>
                                 <td>${contacto.email}</td>
                              </tr>`
          })
          tbody.innerHTML = detalleTabla
        }
}

document.addEventListener("DOMContentLoaded", cargoGrillaContactos(arrayContactos))