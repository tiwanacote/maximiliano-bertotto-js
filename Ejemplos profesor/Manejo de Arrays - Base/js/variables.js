const areaResidencia = [" ", "Área Urbana", "Conurbano", "Pequeña ciudad/pueblo", "Barrio privado"]
//                       0         1             2                   3                   4

const paises = [" ", "Argentina", "Uruguay", "Chile", "Colombia", "Venezuela", "México", "Guatemala"]
//              0         1          2         3         4            5           6           7

let nombreCompleto = "Coderhouse - coding school"

//console.table(areaResidencia) //Visualizar todos los elementos de un array en la Consola JS

//paises.length //Propiedad que nos devuelve el total numérico de elementos de un array

function recorrerPaises() {
    //debugger
    for (let i = 0; i < paises.length; i++) {
        console.log(paises[i])
    }
}

function agregarOtroPais(nuevoPais) {
    //debugger
    if (nuevoPais.trim() > "") {
        paises.push(nuevoPais)
        console.table(paises)
    }
}

function agregarOtroPaisAlInicio(nuevoPais) {
    //debugger
    if (nuevoPais.trim() > "") {
        paises.unshift(nuevoPais)
        console.table(paises)
    }
}

//.pop() elimina un elemento del array, siempre es el último.

//.shift() elimina un elemento del array, en este caso, el primero.

function eliminarUltimoElemento() {
    let elementoEliminado = paises.pop()
        console.warn("Se ha eliminado correctamente el país:", elementoEliminado)
}

function fusionarArrays() {
    debugger
    const paisesAmerica = ["Argentina", "Chile", "Uruguay", "Venezuela"]
    const paisesEuropa = ["Italia", "España", "Francia"]
    
    const paises = paisesAmerica.concat(paisesEuropa)
    console.table(paises)
}

function buscarYeliminar() {
    debugger
    const borrarElem = prompt("Ingrese el país que desea quitar:")
    const indice = paises.indexOf(borrarElem)
        if (indice > -1) {
            paises.splice(indice, 1)
        } else {
            console.warn("El elemento ingresado no fue encontrado: ", borrarElem)
        }
    console.table(paises)       
}