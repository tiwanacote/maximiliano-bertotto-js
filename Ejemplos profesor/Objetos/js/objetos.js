//########################################
//EJEMPLO DE OBJETO LITERAL
// let nombre = "Coder"
// let apellido = "House"
// let edad = 7
// let domicilio = "Dentro de Zoom 123"

// const persona1 = {nombre: "Coder", apellido: "House", edad: 7, domicilio: "Dentro de Zoom 123"}
// const persona2 = {nombre: "Pepe", apellido: "Argento", edad: 55, domicilio: "Barrio de Flores"}
//########################################

//########################################
// EJEMPLO DE PRODUCTOS MEDIANTE FUNCION CONSTRUCTORA
//FUNCTIÓN CONSTRUCTORA
// class producto {
//     constructor(nombre, stock, precio, iva) {
//         this.nombre = nombre
//         this.stock = stock
//         this.precio = precio
//         this.iva = iva
//         this.saludar = function () {
//             console.log("Hola, me llamo " + this.nombre + ".")
//         }
//         this.precioConIVA = function () {
//             if (iva == undefined || iva == 0 || iva == "" || iva == null) {
//                 return console.error("El impuesto al valor agregado no fue configurado.")
//             } else {
//                 return console.log((Number(precio) * Number(iva)).toFixed(2))
//             }
//         }
//         this.desc10off = function () {
//             return (precio * 0.9).toFixed(2)
//         }
//     }
// }

// const producto1 = new producto("Macbook Air", 1, 950, 1.21)
// const producto2 = new producto("EXO i3", 1, 600, 1.105)
//########################################

//########################################
//EJEMPLO DE PRODUCTOS MEDIANTE UNA CLASE CONTRUCTORA
class Producto {
    constructor(pid, nombre, stock, umbral, precio, iva) {
        this.productoid = pid
        this.nombre = nombre
        this.stock = stock
        this.umbral = umbral 
        this.precio = precio
        this.iva = parseFloat(iva)
    }
    descuentoStock(unidades) {
        this.stock -= unidades
        console.log("Se descontó " + unidades + " unidad(es) del stock. Quedan " + this.stock + " unidad(es).")
    }
    umbralDeStock() {
        if (this.umbral > this.stock) {
            console.warn("Debe realizar un nuevo pedido.")
        } else {
            console.info("El stock de productos es óptimo.")
        }
    }
    precioConIVA() {
        let valor = this.iva
        if (valor == undefined || valor == 0 || valor == "" || valor == null) {
            return console.error("El impuesto al valor agregado no fue configurado.")
        } else {
            return console.log((Number(this.precio) * Number(this.iva)).toFixed(2))
        }
    }
    desc10off() {
        return parseFloat((this.precio * 0.9).toFixed(2))
    }
}
//########################################

//Este ejemplo 👇 funciona tanto con la Función Constructora
//Como también con la Clase, anteriormente mostrada 👆
const producto1 = new Producto(22549032, "Macbook Air", 15, UMBRAL, 950, IVALOCAL)
const producto2 = new Producto(22549033, "EXO i3", 50, UMBRAL, 600, IVAIMPORTADO)
const producto3 = new Producto(22549034, "Lenovo i7", 20, UMBRAL, 1200, IVALOCAL)
const producto4 = new Producto(22549035, "Dell", 15, UMBRAL, 975, IVAIMPORTADO)
const producto5 = new Producto(22549036, "Surface", 21, UMBRAL, 1420, IVALOCAL)