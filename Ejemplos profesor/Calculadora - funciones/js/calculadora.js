//################################################################
//OPCION UNO: USANDO DOS FUNCIONES CON SOLO PARÁMETROS
// function calculadora() {
//     let primerNro = prompt("Ingrese el primer número:")
//     let segundoNro = prompt("Ingrese el segundo número:")
//         obtenerCalculo(primerNro, segundoNro)
// }


// function obtenerCalculo(primerNro, segundoNro) {
//     resultado = primerNro + segundoNro
//     console.log("El resultado del cálculo, es: ", resultado)
// }
//################################################################


//################################################################
//OPCION DOS: USANDO DOS FUNCIONES CON PARÁMETROS Y RETORNO
// function calculadora() {
//     let primerNro = prompt("Ingrese el primer número:")
//     let segundoNro = prompt("Ingrese el segundo número:")
//         if (!isNaN(primerNro) && !isNaN(segundoNro)) {
//             let resultado = obtenerCalculo(primerNro, segundoNro)
//             console.log("El resultado del cálculo, es: ", resultado)
//         } else {
//             console.error("Error: ¡Ambos valores ingresados deben ser numéricos!")
//         }
// }


// function obtenerCalculo(primerNro, segundoNro) { 
//     resultado = parseInt(primerNro) + parseInt(segundoNro)
//     return resultado
//     //Se puede optimizar a una sola línea. Ya sabemos que lo que llegue, será numérico
// }
//################################################################


//################################################################
// OPCION TRES: CALCULADORA CON TODOS LOS OPERADORES
function calculadora() {
    let primerNro = prompt("Ingrese el primer número:")
    let segundoNro = prompt("Ingrese el segundo número:")
    let operador = prompt("Tipo de operación a realizar: \n SUMA = + \n RESTA = - \n MULTIPLICACIÓN = * \n DIVISIÓN = /")
        if (!isNaN(primerNro) && !isNaN(segundoNro)) {
            let resultado = obtenerCalculo(primerNro, segundoNro, operador)
            console.log("El resultado del cálculo, es: ", resultado)
        } else {
            console.error("Error: ¡Ambos valores ingresados deben ser numéricos!")
        }
}

function obtenerCalculo(primerNro, segundoNro, operador) { 
    //debugger
    if (operador != null && operador != undefined && operador != "") {
        switch (operador) {
            case "+":
                return parseInt(primerNro) + parseInt(segundoNro)
            case "-":
                return parseInt(primerNro) - parseInt(segundoNro)
            case "*":
                return parseInt(primerNro) * parseInt(segundoNro)
            case "/":
                return parseInt(primerNro) / parseInt(segundoNro)
            // O, en lugar del if(), agregar aquí la opción default: (y eliminar el else())                
        }
    } else {
        return "¡Error!"
    }
}
//################################################################