const comboProd = document.getElementById("selectProduct")
const sku = document.getElementById("sku")
const skuQuantity = document.getElementById("skuQ")
const resumenCompra = document.getElementById("resumen")
const productos = []

function cargarSelectProduct() {
    comboProd.innerHTML += `<option disabled selected value> -- select an option -- </option>`
    for (let el = 0; el < productSKUs.length; el++)
        comboProd.innerHTML += `<option value="${el}">${productSKUs[el]}</option>`
}

//debugger
cargarSelectProduct()

// Evento de selección de producto
comboProd.onchange = (event) => {
    var inputProd = productSKUs[event.target.value];
    console.log(inputProd);
    quantity = prompt("Ingrese la cantidad que requiere de este producto");
    console.log(quantity);

    // if ((quantity != null) && (Number.isInteger(quantity)))  {
    if (quantity != null)   {
        sku.innerHTML = "Producto seleccionado: " + inputProd;
        skuQuantity.innerHTML = "Cantidad: " + quantity;
      }
}

// Evento boton "Agregar producto"
function addProduct() {
    const costo = quantity * priceSKUs[productSKUs[comboProd.value]];
    addedProd.push({"Producto": productSKUs[comboProd.value] , "Importe": costo});
    console.table(addedProd)
    sku.innerHTML = "PRODUCTO AGREGADO";
    skuQuantity.innerHTML = "Agregue otro producto o presione el boton COMPRAR para finalizar";
}

// Evento boton "Comprar"
function checkOut(){
    let total = 0;
    for (let el = 0; el < addedProd.length; el++){
        total = total + addedProd[el].Importe
    }
    resumenCompra.innerText = "Total compra: " + total;
}


