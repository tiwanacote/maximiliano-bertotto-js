// Función que contiene el código HTML para llenar las tarjetas de productos
function HTMLcardsContainer(elem, counter, doSpecial){
    code = `<div class="card" style="width: 18rem;">
    <img src="${elem.data.picture_path}" class="card-img-top" alt="${elem.sku_name}">
        <div class="card-body">
            <h5 class="card-title">${elem.sku_name}</h5>
            <p class="card-text">${elem.data.description}</p>
            <a href="#" class="btn btn-primary" id="btnAdd-${elem.id}" onclick="eventAddProd()">Agregar</a>
        </div>
    </div>`;
    return code;
}

// Función que contiene la plantilla para llenar el carrito de productos
function HTMLorderTable(elem, counter , doSpecial){
    if (doSpecial != counter){
        code = `<tr>
                    <th scope="row">${counter}</th>
                    <td>${elem.id}</td>
                    <td>${elem.sku_name}</td>   
                    <td ><input class="inputQuantity" id="input-${elem.id}" onchange="eventInputQuantity()"  type="number" value="${elem.quantity}" min="1" ></td>
                    <td>${(elem.price.cash * (elem.price.tax/100 + 1)).toFixed(2)} $</td>
                    <td>${(elem.price.cash * (elem.price.tax/100 + 1) * elem.quantity).toFixed(2)} $</td>
                    <td><button type="button" id="btnDel-${elem.id}" class="btn btn-default btn-circle" onclick="eventDeleteRow()"><i class="fa fa-times" id="x-${elem.id}"></i></button></td>
                </tr>`;
    }
    else{
        doSpecial==1? no_prod = "No hay productos seleccionados" : no_prod = ""
        console.log("doSpecial" + doSpecial)
        code = `<tr class="cartSubTotal">
                    <th scope="row"></th>
                    <td></td>
                    <td>${no_prod}</td>   
                    <td></td>
                    <td class="sub"><strong>Sub-Total</td>
                    <td class="sub" id="totalCostCart"><strong>${elem.totalCostCart.toFixed(2)}$</td>
                    <td></td>
                </tr>`;
    }
    return code;
}

// Función que contiene el código HTML para llenar las el select input de provincias
function HTMLdeliveryProv(elem, counter, doSpecial){
    code = counter==1? 
        `<option selected>Provincia</option><option value=${counter}>${elem}</option>` : 
        `<option value=${counter}>${elem}</option>`
    return code;
}

// Función que contiene el código HTML para llenar las el select input de localidades
function HTMLdeliveryLoc(elem, counter, doSpecial){
    code = counter==1?
        `<option selected>Localidad</option><option value=${counter}>${elem}</option>` :
        `<option value=${counter}>${elem}</option>`
    return code;
}
