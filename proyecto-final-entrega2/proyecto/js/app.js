// DOM elements
const radioInput1     = document.getElementById("radioInput1")
const radioInput2     = document.getElementById("radioInput2")
const addressFormsId  = document.getElementById('addressFormsId')
const provinciaFormId = document.getElementById('provinciaFormId')
const localidadFormId = document.getElementById('localidadFormId')
const costDeliveryId  = document.getElementById('costDeliveryId')

let storedProductCart  =  JSON.parse( localStorage.getItem('productCart')) || {};

const SKU_URL  = "js/skus.json"
const PROV_URL = "js/provinces.json"
const LOC_URL  = "js/localities.json"

let orderCart        = new OrderCart(storedProductCart);
const productCards   = new HTMLbuilder([] , "skuContainer" , HTMLcardsContainer  );
const orderTable     = new HTMLbuilder(orderCart.getList4HTML() , "orderTableBody" , HTMLorderTable  );
const orderDelivProv = new HTMLbuilder([] , "provinciaFormId" , HTMLdeliveryProv  );
const orderDelivLoc  = new HTMLbuilder([] , "localidadFormId" , HTMLdeliveryLoc  );


const getLocalities = (key , data) =>{
    for (let elem of data) {
        if (elem.provincia==key){
            let localities = [];
            for (let i = 0; i < elem.localidad.length; i++)
                localities.push(elem.localidad[i].nombre);
            return localities;
        }
    };
    return []
}

const getData = (url, obj, key=null) => {
    let temp = []
    fetch(url)
        // .then(obj.HTMLloading())
        .then(response=>response.json())
        .then((data)=> key? temp = getLocalities(key, data) : temp=data)
        .catch(error => temp=[])
        .finally(()=>obj.HTMLrefresh(temp))
}

productCards.HTMLbuild();
orderTable.HTMLbuild();
getData(SKU_URL, productCards);
getData(PROV_URL, orderDelivProv);