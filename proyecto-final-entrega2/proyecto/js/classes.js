// Clase abstracta para llenar cualquier elemento con código HTML
class HTMLbuilder {

    constructor(elemList, HTMLid, HTMLcode) {
        this.elemList = elemList;                           // Lista a iterar
        this.HTMLelem = document.getElementById(HTMLid);    // Elemento HTML a llenar
        this.HTMLcode = HTMLcode;                           // Función con código HTML a pegar
    }

    HTMLbuild() {
        let counter = 0;
        let doSpecial = this.elemList.length;
        this.HTMLelem.innerHTML = '';
        if (this.elemList.length > 0) {
            for (let elem of this.elemList) {
                counter += 1;
                this.HTMLelem.innerHTML += this.HTMLcode(elem, counter, doSpecial);
            }
        }
    }

    HTMLrefresh(list) {
        this.elemList = list;
        this.HTMLbuild();
    }

    HTMLloading() {
        this.HTMLelem.innerHTML = HTML_LOADING; 
    }
}


// Clase con métodos y atributos relacionados a la orden puesta por el cliente
class OrderCart {
    constructor(prodCart = {}, catalog = skus_3) {
        this.catalog = catalog;
        // this.catalog   = catalog || [{"error":"No se pudo cargar los datos"}];
        this.prodCart = prodCart;
        this.list4HTML = this._generateList4HTML(this.catalog); // Lista de SKUs con cantidades para poder generar tabla HTML
    }

    // Calcula los costos del carrito de productos
    _calculateCartCost(prodCart, catalog) {
        let cartCost = 0;
        for (let [key, value] of Object.entries(prodCart)) {
            let obj = catalog.find(o => o.id === key);
            cartCost += obj.price.cash * (1 + obj.price.tax / 100) * value;
        }
        return cartCost;
    }

    // Calcula costos de envío
    calcDeliveryCosts(){
        let delivCost = 0;
        let volWeight = 0;
        let isSharableBox = false;
        const PRICE = 1.75; // Provisorio
        for (let [key, quantity] of Object.entries(this.prodCart)) {
            let obj = this.catalog.find(o => o.id === key);
            // NO comparte caja y suma el volumen al envío (Caso de máquina o filamento)
            if (!obj.delivery.comparteCaja)
                volWeight += obj.delivery.pesoVolumetrico * quantity;
            // Comparte caja y no suma al volumen del envío, salvo que sea el primer envío pequeño.
            else{
                if (!isSharableBox){
                    volWeight += obj.delivery.pesoVolumetrico;
                    isSharableBox=true;
                }
            }
        }
        delivCost = volWeight * 1.75;
        return delivCost;
    }

    _generateList4HTML(skus) {
        let output = [];
        for (let [key, value] of Object.entries(this.prodCart)) {
            let obj = skus.find(o => o.id === key);   // Busca el objeto que tiene ID=KEY y lo devuelve entero    
            Object.assign(obj, { quantity: value });
            output.push(obj);
        }
        output.push({ totalCostCart: this._calculateCartCost(this.prodCart, this.catalog) });
        return output;
    }

    _localStorage(nameKey, obj) {
        localStorage.setItem(nameKey, obj)
    }

    _withoutStockHTML() {
        Swal.fire({
            title: 'Sin Stock!',
            text:  'Lo sentimos, estamos sin stock',
            icon:  'warning',
            confirmButtonText: 'OK'
        });
    }
    _invalidQuantity() {
        Swal.fire({
            title: 'Error al ingresar cantidad',
            text:  'El número ingresado debe ser mayor a 0',
            icon:  'error',
            confirmButtonText: 'OK'
        });
    }

    // Calcula nuevamente las cantidades por SKU ID
    refreshProdCart() {
        this.list4HTML = this._generateList4HTML(this.catalog);
    }

    // Agrega producto 
    addOneProduct(prodID) {
        let prod = this.catalog.find(o => o.id === prodID);
        // Verifico si el stock del SKU es >= al la cantidad de productos solcitada
        const stockOk = (prod.stock >= (this.prodCart[prodID] || 0) + 1) ? true : false
        if (stockOk) {
            this.prodCart[prodID] = (this.prodCart[prodID] || 0) + 1;
            this.refreshProdCart();
            this._localStorage('productCart', JSON.stringify(this.prodCart));
        } else {
            this._withoutStockHTML();
        }
    }

    addProducts(prodID, quantity) {
        if (quantity > 0) {
            let jsonProd = this.catalog.find(o => o.id === prodID);
            // Verifico si el stock del SKU es >= al la cantidad de productos solcitada
            const stockOk = (jsonProd.stock >= quantity) ? true : false
            if (stockOk) 
                this.prodCart[prodID] = quantity;
            else {
                this.prodCart[prodID] = jsonProd.stock;
                this._withoutStockHTML();
            }
            this.refreshProdCart();
            this._localStorage('productCart', JSON.stringify(this.prodCart));
        }else
            this._invalidQuantity();
    }

    deleteProduct(prodID) {
        delete this.prodCart[prodID];
        this.refreshProdCart();
        this._localStorage('productCart', JSON.stringify(this.prodCart));
    }

    getList4HTML() {
        return this.list4HTML;
    }

    getProductCart(){
        return this.prodCart;
    }
}


