// ------------------ Product Cart -------------------------
const eventAddProd = () =>{
    const prodId = event.target.id.split("-")[1];
    orderCart.addOneProduct(prodId);
    orderTable.HTMLrefresh(orderCart.getList4HTML());
};

const eventInputQuantity = () =>{
    const prodId = event.target.id.split("-")[1];
    prodQuantity = event.target.value;
    orderCart.addProducts(prodId , prodQuantity);
    orderTable.HTMLrefresh(orderCart.getList4HTML());
}

const eventDeleteRow = () =>{
    const prodId = event.target.id.split("-")[1];
    orderCart.deleteProduct(prodId);
    orderTable.HTMLrefresh(orderCart.getList4HTML()); 
}

// ------------------ Delivery -------------------------

radioInput1.addEventListener("click", function() {
    provinciaFormId.disabled=true;
    localidadFormId.disabled=true;
});

radioInput2.addEventListener("click", function() {
    provinciaFormId.disabled=false;
    provinciaFormId.value=="Provincia"? localidadFormId.disabled=true : localidadFormId.disabled=false;
});

provinciaFormId.addEventListener("change", function(event) {
    provinciaFormId.value=="Provincia"? localidadFormId.disabled=true : localidadFormId.disabled=false;
    try{
        const province = event.target.options[event.target.value].innerHTML;
        getData(LOC_URL, orderDelivLoc, province );
    }catch(error){
        localidadFormId.value="Localidad"
        costDeliveryId.innerText="--- $";
    }
});

localidadFormId.addEventListener("change", function(event) {
    try{
        const localidad = event.target.options[event.target.value].innerHTML;
        costDeliveryId.innerText=orderCart.calcDeliveryCosts() + " $";
    }catch(error){
        localidadFormId.value="Localidad"
        costDeliveryId.innerText="--- $";
    }
});

