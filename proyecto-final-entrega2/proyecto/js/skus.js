
const skus_3 =
    [


        {
            id: "1000",
            category: "maquinas",
            sub_category: "semi_cerradas",

            sku_name: "Cosmos II",

            stock: 1,
            price:
            {
                cash: 134800,
                price_12c: 13705,
                price_18c: 9586,
                price_24c: 7526,
                tax: 10.5
            },
            data:
            {
                description: "La máquina profesional más por universidades y PYMES",
                picture_path: "./img/sku/cosmos2.png"
            },
            delivery:
            {
                pesoVolumetrico:30,
                comparteCaja:false
            }

        },



        {
            id: "2000",
            category: "maquinas",
            sub_category: "abiertas",

            sku_name: "Nebula Plus",

            stock: 2,
            price:
            {
                cash: 66900,
                price_12c: 6802,
                price_18c: 4757,
                price_24c: 3735,
                tax: 10.5
            },
            data:
            {
                description: "La máquina ideal para emprender",
                picture_path: "./img/sku/nebulaPlus.png"
            },
            delivery:
            {
                pesoVolumetrico:30,
                comparteCaja:false
            }

        },


        {
            id: "1001",
            category: "componentes",
            sub_category: "extrusor",

            sku_name: "Barrel Cosmos II",

            stock: 5,
            price:
            {
                cash: 435,
                price_12c: 255,
                price_18c: 166,
                price_24c: 90,
                tax: 21
            },
            data:
            {
                description: "Barrel de acero inoxidable. Rosca M6, largo 21mm. Incluye teflon.",
                picture_path: "./img/sku/nebulaPlus.png"
            },
            delivery:
            {
                pesoVolumetrico:30,
                comparteCaja:true
            }

        },


        {
            id: "2002",
            category: "componentes",
            sub_category: "cama caliente",

            sku_name: "Cama magnetica Nebula Plus",

            stock: 4,
            price:
            {
                cash: 10300,
                price_12c: 755,
                price_18c: 509,
                price_24c: 444,
                tax: 21
            },
            data:
            {
                description: "Cama caliente de acero inoxidable de 0.5mm de espesor con superficie de PEI. Incluye imán auto_adhesivo",
                picture_path: "./img/sku/nebulaPlus.png"
            },
            delivery:
            {
                pesoVolumetrico:30,
                comparteCaja:true
            }

        }
    ]

errorSku = [{
    id: "---",
    category: "---",
    sub_category: "---",

    sku_name: "---",

    stock: 1,
    price:
    {
        cash: 0,
        price_12c: 0,
        price_18c: 0,
        price_24c: 0,
        tax: 0
    },
    data:
    {
        description: "---",
        picture_path: "./img/sku/cosmos2.png"
    }

}]
