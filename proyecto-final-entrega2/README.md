NODE JS: Cómo generar un servidor  

1) Llevar el proyecto (El CSS, HTML, JS) a una carpeta que podría llamarse “project”  
2) Por fuera de esa carpeta crear el archivo de entrada, por ejemplo “index.js”  
3) Inicializar el repo: npm init y se creará un archivo json de configuraciones  
4) Instalar EXPRESS: npm i express  que es para crear fácilmente un servidor. Copiar de la página el texto que se debe de pegar en index.js (Entrar a la web npmjs.com y buscar express)  
5) Instalar Nodemon: npm i nodemon que es como el Live Server, para no tener que cargar todo el tiempo  
6) Poner en consola nodemon index.js. Si no llega a funcionar, editar el json con:
"scripts": { "serve": "nodemon server.js" },
y luego correr el servidor de esta manera: npm run serve 


LINKS de INTERES: 

Interesante: Attach multiple elements to a single event listener  
https://stackoverflow.com/questions/8922002/attach-event-listener-through-javascript-to-radio-button

Código prolijo:  
https://github.com/ryanmcdermott/clean-code-javascript#introduction  


