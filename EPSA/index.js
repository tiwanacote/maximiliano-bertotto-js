// ----------------------------------------------------------------------
// De aquí en adelante funciona. Se trabaja desde NODE JS.
// ----------------------------------------------------------------------

// Consulta de Servicios
// const url = 'https://epresis.epsared.com.ar/api/v2/serviciosByCliente.json'
// const body = {"api_token":"CSc3OtUN2fyO1DXUZ06vFa1WbzzhNCQWxfYsAN7z4YZI44g1twgITgtXfOAR"}

// Cotizador de envios
const url = 'https://epresis.epsared.com.ar/api/v2/cotizador.json'
const body = { "api_token": "CSc3OtUN2fyO1DXUZ06vFa1WbzzhNCQWxfYsAN7z4YZI44g1twgITgtXfOAR", "codigo_servicio": "187", "cp_origen": "1001", "cp_destino": "1001", "tiempo": "24 HS", "destino": "AMBA", "is_urgente": false, "valor_declarado": 100.25, "productos": [{ "bultos": 2, "peso": 2, "descripcion": "Caja de zapatos roja", "dimensiones": { "alto": 0.25, "largo": 0.25, "profundidad": "0.25" } }] }

// Dummy Test
// const body = {"api_token":"CSc3OtUN2fyO1DXUZ06vFa1WbzzhNCQWxfYsAN7z4YZI44g1twgITgtXfOAR"}
// const url = 'https://epresis.epsared.com.ar/api/v2/dummy-test.json'

import fetch from 'node-fetch';
const response = await fetch(url, {
  method: 'post',
  body: JSON.stringify(body),
  headers: { 'Content-Type': 'application/json' }
});

const data = await response.json();

console.log("Data obtenida de EPSA:");
console.log(data);


// POST cualquiera
// import fetch from 'node-fetch';

// const body = {a: 1};

// const response = await fetch('https://httpbin.org/post', {
// 	method: 'post',
// 	body: JSON.stringify(body),
// 	headers: {'Content-Type': 'application/json'}
// });
// const data = await response.json();

// console.log("MAXI data");
// console.log(data);