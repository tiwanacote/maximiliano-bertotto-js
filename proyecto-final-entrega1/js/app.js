const comboProd = document.getElementById("selectProduct")
const sku = document.getElementById("sku")
const skuQuantity = document.getElementById("skuQ")
const resumenCompra = document.getElementById("resumen")
const delLastProdBut = document.getElementById("delLastProd")
const checkOutBut = document.getElementById("checkOut")

const productos = []
// Se crea un objeto de lista de pedidos realizados por un cliente
let listaPedido = new Pedido(priceSKUs , productSKUs)

function cargarSelectProduct() {
    comboProd.innerHTML += `<option disabled selected value> -- select an option -- </option>`
    for (let el = 0; el < productSKUs.length; el++)
        comboProd.innerHTML += `<option value="${el}">${productSKUs[el]}</option>`
}

//debugger
cargarSelectProduct()

// Evento de selección de producto
comboProd.onchange = (event) => {
    var inputProd = productSKUs[event.target.value];
    console.log(inputProd);
    quantity = prompt("Ingrese la cantidad que requiere de este producto");
    console.log(quantity);

    if (quantity != null)   {
        sku.innerHTML = "Producto seleccionado: " + inputProd;
        skuQuantity.innerHTML = "Cantidad: " + quantity;
      }
}

// Evento boton "Agregar producto"
function addProduct() {
    listaPedido.agregarProducto(comboProd.value , quantity )
    // const costo = quantity * priceSKUs[productSKUs[comboProd.value]];
    // addedProd.push({"Producto": productSKUs[comboProd.value] , "Importe": costo});
    // console.table(addedProd)

    // Agregamos nuevo texto en DOM
    sku.innerHTML = "PRODUCTO AGREGADO";
    skuQuantity.innerHTML = "Agregue otro producto o presione el boton COMPRAR para finalizar";
    // Cambiamos los estilos del boton si hay un elemento que se puede comprar
    checkOutBut.style.backgroundColor = "#08769B";
    checkOutBut.style.borderColor = "#08769B";
    checkOutBut.style.pointerEvents = "auto";
}


// Evento boton "Comprar"
function checkOut(){
    // Muestro el boton de borrar
    delLastProdBut.style.backgroundColor = "#c56565";
    delLastProdBut.style.borderColor = "#c56565";
    delLastProdBut.style.pointerEvents = "auto";

    // Calculo el valor del carrito
    listaPedido.calcularPrecioFinal();
    resumenCompra.innerText = "Total compra: " + listaPedido.costoFinal;
    
}

function delProduct(){
    // Borro producto
    listaPedido.borrarUltimoProducto();
    checkOut();
}


