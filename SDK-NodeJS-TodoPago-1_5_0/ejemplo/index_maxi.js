import fetch from 'node-fetch';

// GET CREDENTIALS TodoPago
// const url = 'https://developers.todopago.com.ar/api/Credentials'
const url = 'https://developers.todopago.com.ar/'
const body = { "USUARIO": 'info@trimaker.com', "CLAVE": 'Tr1m4k3rpago' }

// You might have guessed it already: it's a connection error.
// "ECONNRESET" means the other side of the TCP conversation abruptly closed its end of the
//  connection. This is most probably due to one or more application protocol errors. You 
// could look at the API server logs to see if it complains about something.

try {
	const response = await fetch(url, {
        method: 'post',
        // body: JSON.stringify(body),
        body: body,
        headers: { 'Content-Type': 'application/json' }
    });
    
    const data = await response.json();
    
    console.log("Data obtenida de TODOPAGO:");
    console.log(data);
} catch (error) {
	console.log(error);
}